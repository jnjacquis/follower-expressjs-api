const express = require('express')
const cors = require('cors')

const app = express()
const port = 4000

let followersCount = 0;

function incFollowersCount() {
    followersCount = followersCount + 1;
    displayCount();
}

function decFollowersCount() {
    followersCount = followersCount - 1;
    if (followersCount < 0) {
        followersCount = 0;
    }
    displayCount();
}

function displayCount() {
    console.log('Followers #' + followersCount);
}

app.use(cors())

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

app.get('/', function (req, res) {
    res.status(404).send('Sorry cant find that!');
})

app.post('/follow', function (req, res) {
    let id = req.query.id;
    console.log('Follow item with id ' + id);
    incFollowersCount();
    res.send(true);
})

app.post('/unfollow', function (req, res) {
    let id = req.query.id;
    console.log('Unfollow item with id ' + id);
    decFollowersCount();
    res.send(true);
})
